package com.brevitaz.demo.streamapi;

import com.brevitaz.demo.streamapi.models.Customer;
import com.brevitaz.demo.streamapi.models.Order;
import com.brevitaz.demo.streamapi.models.Product;
import com.brevitaz.demo.streamapi.repos.CustomerRepo;
import com.brevitaz.demo.streamapi.repos.OrderRepo;
import com.brevitaz.demo.streamapi.repos.ProductRepo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
@RunWith(SpringRunner.class)
@DataJpaTest
public class StreamApiTest {

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private ProductRepo productRepo;


    @Test
    @DisplayName("DEMO TEST: Obtain a list of all product")
    public void exercise0() {

        final List<Product> products = productRepo.findAll()
                .stream()
                .collect(Collectors.toList());

        log.info("List of all products: \n {}", products);

    }

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100")
    public void exercise1() {

        final String productCategory = "Book";
        final int productPrice = 100;
        Predicate<Product> condition = (product1) ->  product1.getCategory()
                .equalsIgnoreCase(productCategory) && product1.getPrice() > productPrice;

        List<Product> products = productRepo.findAll()
                                            .stream()
                                            .filter(condition)
                                            .toList();

        log.info("List of product with category = \"Books\" and price > 100 -> {}", products );
    }

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100 (using Predicate chaining for filter)")
    public void exercise1a() {
        final String productCategory = "Book";
        final int productPrice = 100;
        List<Product> products = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory().equalsIgnoreCase(productCategory))
                .filter(product -> product.getPrice() > productPrice)
                .toList();

        log.info("List of product with category = \"Books\" and price > 100 -> {}", products );
    }

    @Test
    @DisplayName("Obtain a list of product with category = \"Books\" and price > 100 (using BiPredicate for filter)")
    public void exercise1b() {
        final String productCategory = "Book";
        final int productPrice = 100;

        BiPredicate<String, String> isStringEqual = String::equalsIgnoreCase;
        BiPredicate<Double, Integer> findGreater = (integer, integer2) -> integer > integer2;

        List<Product> products = productRepo.findAll()
                .stream()
                .filter((product) -> findGreater.test(product.getPrice(), productPrice) && isStringEqual.test(product.getCategory(), productCategory))
                .filter(product -> findGreater.test(product.getPrice(), productPrice))
                .toList();
        log.info("List of product with category = \"Books\" and price > 100 (using BiPredicate for filter) -> {}", products );
    }

    @Test
    @DisplayName("Obtain a list of order with product category = \"Baby\"")
    public void exercise2() {
        final String productCategory = "Baby";

        Set<Order> orders =
                productRepo.findAll()
                .stream()
                .filter(product1 -> product1.getCategory().equalsIgnoreCase(productCategory))
                .flatMap(product -> product.getOrders().stream())
                .collect(Collectors.toSet());

        Set<Order> orders2 = orderRepo.findAll()
                .stream()
                .filter(order -> !order.getProducts().stream().filter(product -> productCategory.equalsIgnoreCase(product.getCategory())).collect(Collectors.toSet()).isEmpty())
                        .collect(Collectors.toSet());


                log.info("List of order with product category = \"Baby\"-> {}", orders);
    }

    @Test
    @DisplayName("Obtain a list of product with category = “Toys” and then apply 10% discount\"")
    public void exercise3() {
        final String productCategory = "Toys";

        List<Product> products = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory()
                        .equalsIgnoreCase(productCategory))
                .map(product -> {
                    product.setPrice(product.getPrice() - (product.getPrice()*10)/100);
                        return product;
                })
                .collect(Collectors.toList());

        log.info("list of product with category = “Toys” and then apply 10% discount-> {}", products);
    }

    @Test
    @DisplayName("Obtain a list of products ordered by customer of tier 2 between 01-Feb-2021 and 01-Apr-2021")
    public void exercise4() {
        final LocalDate firstDate = LocalDate.of(2021,1,31);
        final LocalDate lastDate = LocalDate.of(2021,4,2);

        Set<Product> products = Optional.of(orderRepo.findAll()
                .stream()
                .filter(order -> order.getCustomer().getTier() == 2)
                .filter(order -> order.getOrderDate()
                        .isAfter(firstDate) && order.getOrderDate()
                        .isBefore(lastDate))
                .flatMap(order -> order.getProducts().stream()).collect(Collectors.toSet())).orElseGet(Collections::emptySet);
                //change.......................................................................................
//                .map(Order::getProducts);

        log.info("list of products ordered by customer of tier 2 between 01-Feb-2021 and 01-Apr-2021-> {}", products);
    }

    @Test
    @DisplayName("Get the 3 cheapest products of \"Books\" category")
    public void exercise5() {
        final String productCategory = "Books";

        List<Product> products = productRepo.findAll()
                .stream()
                .filter(product -> product.getCategory().equalsIgnoreCase(productCategory))
                .sorted((o1, o2) -> {
                    if (o1.getPrice() > o2.getPrice())
                        return 1;
                    else if (o1.getPrice() < o2.getPrice())
                        return -1;
                    else
                        return 0;
                })
                .limit(3)
                .toList();

        log.info("list of 3 cheapest products of \"Books\" category-> {}", products);
    }
// Continue here ..............................................................................................................................
    @Test
    @DisplayName("Get the 3 most recent placed order")
    public void exercise6() {
        List<Order> orders = orderRepo.findAll()
                .stream()
                .sorted(Comparator.comparing(Order::getOrderDate).reversed())
                .sorted((o1, o2) -> {
                    if (o1.getOrderDate().isBefore(o2.getOrderDate()))
                        return 1;
                    else if (o1.getOrderDate().isAfter(o2.getOrderDate()))
                        return -1;
                    else
                        return 0;
                })
                .limit(3)
                .toList();

        log.info("list of 3 most recent placed orders -> {}", orders);
    }

    @Test
    @DisplayName("Get a list of products which was ordered on 15-Mar-2021")
    public void exercise7() {
        final LocalDate date = LocalDate.of(2021, 3, 15);

        List<Product> products = productRepo.findAll()
                .stream()
                .filter(product -> product.getOrders()
                        .stream()
                        .anyMatch(order -> order.getOrderDate().isEqual(date)))
                .collect(Collectors.toList());

//                orderRepo.findAll()
//                .stream()
//                .filter(order -> order.getOrderDate().isEqual(date))
//                .flatMap(o -> o.getProducts().stream())
//                        .collect(Collectors.toList());

        log.info("list of products which was ordered on 15-Mar-2021 -> {}", products);
    }

    @Test
    @DisplayName("Calculate the total lump of all orders placed in Feb 2021")
    public void exercise8() {
        final Month month = Month.FEBRUARY;
        final int year = 2021;

        Long orderCount = orderRepo.findAll()
                .stream()
                .filter(order -> order.getOrderDate()
                        .getYear() == year && order.getOrderDate()
                                                    .getMonth().equals(month))
                .count();

        log.info("The total lump of all orders placed in Feb 2021 -> {}", orderCount);
    }

    @Test
    @DisplayName("Calculate the total lump of all orders placed in Feb 2021 (using reduce with BiFunction)")
    public void exercise8a() {
        final Month month = Month.FEBRUARY;
        final int year = 2021;

        BiFunction<Long, Order, Long> function = (aLong, order) -> aLong++;

//        Long orderCount = orderRepo.findAll()
//                .stream()
//                .reduce(0L, function, (aLong, aLong2) -> aLong++);
//
//        log.info("The total lump of all orders placed in Feb 2021 -> {}", orderCount);
    }

    @Test
    @DisplayName("Calculate the average price of all orders placed on 15-Mar-2021")
    public void exercise9() {
        final LocalDate date = LocalDate.of(2021, 3, 15);

        OptionalDouble result = orderRepo.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(date))
                .mapToDouble(order -> order.getProducts()
                                            .stream()
                                            .mapToDouble(Product::getPrice)
                                            .sum())
                .average();

        log.info("The total lump of all orders placed in Feb 2021 -> {}", result);
    }

    @Test
    @DisplayName("Obtain statistics summary of all products belong to \"Books\" category")
    public void exercise10() {
        //skip
    }

    @Test
    @DisplayName("Obtain a mapping of order id and the order's product count")
    public void exercise11() {
        Map<Long, Integer> productCountPerOrderId = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Order::getId, o -> o.getProducts().size()));

        log.info("The mapping of order id and the order's product count -> {}", productCountPerOrderId);
    }

    @Test
    @DisplayName("Obtain a data map of customer and list of orders")
    public void exercise12() {
        Map<Customer, List<Order>> listOfOrderPerCustomer = orderRepo.findAll()
                .stream()
                .collect(Collectors.groupingBy(Order::getCustomer));

        log.info("The a data map of customer and list of orders -> {}", listOfOrderPerCustomer);
    }

    @Test
    @DisplayName("Obtain a data map of customer_id and list of order_id(s)")
    public void exercise12a() {
        Map<Long, List<Long>> idMapping = orderRepo.findAll()
                .stream()
                .collect(Collectors.groupingBy(Order::getCustomer))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey().getId(),
                        entry -> entry.getValue().stream().map(Order::getId).toList()));

        Map<Long, List<Long>> idMapping1 = orderRepo.findAll()
                .stream()
                .collect(Collectors.groupingBy(o -> o.getCustomer().getId(),
                        Collectors.collectingAndThen(Collectors.toList(),
                                                     orders -> orders.stream()
                                                              .map(Order::getId)
                                                             .collect(Collectors.toList()))));
        log.info("The a data map of customer_id and list of order_id(s) -> {}", idMapping);
    }

    @Test
    @DisplayName("Obtain a data map with order and its total price")
    public void exercise13() {
        // Try it with Stream on product
        Map<Order, Double> orderWithTotalPrice = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Function.identity(), order -> order.getProducts()
                        .stream()
                        .mapToDouble(Product::getPrice)
                        .sum()));


        log.info("The data map with order and its total price -> {}", orderWithTotalPrice);
    }

    @Test
    @DisplayName("Obtain a data map with order and its total price (using reduce)")
    public void exercise13a() {
        Map<Order, Double> orderWithTotalPrice = orderRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Function.identity(), order -> order.getProducts()
                                .stream()
                                .map(Product::getPrice)
                                .reduce(0.0, (a1, a2) -> a1+a2)));

        log.info("The data map with order and its total price (using reduce) -> {}", orderWithTotalPrice);
    }

    @Test
    @DisplayName("Obtain a data map of product name by category")
    public void exercise14() {
        Map<String, List<String>> productNameByCategory = productRepo.findAll()
                .stream()
                .collect(Collectors
                        .groupingBy(Product::getCategory,
                                    Collectors.collectingAndThen(Collectors.toList(),
                                                                 products -> products.stream()
                                                                         .map(Product::getName)
                                                                         .collect(Collectors.toList()))));
             //   .collect(Collectors.toMap(Product::getName, Product::getCategory));

        log.info("The data map of product name by category -> {}", productNameByCategory);
    }

    @Test
    @DisplayName("Get the most expensive product per category")
    void exercise15() {
        Map<String,Product> productPerCategory = productRepo.findAll()
                .stream()
                .collect(Collectors.toMap(Product::getCategory, Function.identity(),
                        (o1, o2) -> {
                            if (o1.getPrice() < o2.getPrice())
                                return o2;
                            else
                                return o1;
                        }
                        )
                );

        log.info("The most expensive product per category -> {}", productPerCategory);
    }

    @Test
    @DisplayName("Get the most expensive product (by name) per category")
    void exercise15a() {
        //fix.................................................
        Map<String, String> productPerCategory = productRepo.findAll()
                .stream()
                .collect(Collectors
                .groupingBy(Product::getCategory,
                        Collectors.collectingAndThen(Collectors.toList(),
                                products -> products.stream()
                                        .max(Comparator.comparing(Product::getPrice))
                                        .get()
                                        .getName()
                        )));


               // .collect(Collectors.groupingBy(Product::getCategory), Collectors.mapping(t -> t.getClass()));
//        Map<String,String> productPerCategory = productRepo.findAll()
//                .stream()
//                .collect(Collectors.toMap(Product::getCategory,
//                        Function.identity(),
//                        BinaryOperator.maxBy(Comparator.comparing(Product::getPrice))))
//                .entrySet()
//                .stream()
//                .collect(Collectors.toMap(
//                        entry -> entry.getKey(),
//                        entry -> entry.getValue().getCategory()));

        log.info("The most expensive product (by name) per category -> {}", productPerCategory);
    }
}